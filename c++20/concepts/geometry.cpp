#include <iostream>
#include <concepts>
#include <map>
#include <cmath>
#include <unordered_map>


using namespace std;


template<typename T>
concept Vector =
    requires(T t1, T t2) {
        { t1.Length() };
        { t1.Dot(t2) };
        { T::dimension };
};

template<typename T>
concept Vector2D = Vector<T> && (T::dimension==2);

template<typename T>
concept Vector3D = Vector<T> && (T::dimension==3);

struct Vector2
{
    int x, y;

    double Length() const { return sqrt(x*x + y*y); }
    double Dot(const Vector2& rhs) const { return x*rhs.x + y*rhs.y; }
	constexpr static size_t dimension = 2;
};

struct Point2
{
    int x, y;

    double Length() const { return sqrt(x*x + y*y); }
    double Dot(const Vector2& rhs) const { return x*rhs.x + y*rhs.y; }
	constexpr static size_t dimension = 2;
};

struct Vector3
{
    int x, y, z;

    double Length() const { return 100; }
    double Dot(const Vector3& rhs) const { return 0; }
	constexpr static size_t dimension = 3;
};


template <Vector T>
double cosine(T t1, T t2) 
{
	cout << "calculating cosine of N-dimensional vectors" << endl;
	return t1.Dot(t2)/(t1.Length()*t2.Length());        
}

template <Vector2D T>
double cosine(T t1, T t2)
{
	cout << "calculating cosine of 2D vectors using lightning fast implementation" << endl;
	return 132;        
}

template <Vector2D T>
bool isClockwise(T t1, T t2, T t3, T t4)
{
	cout << "checking if 2d polygon is clockwise" << endl;
	return true;
}

template <Vector3D T>
bool isPlanar(T t1, T t2, T t3, T t4)
{
	cout << "checking if 3d points form planar polygon" << endl;
	return true;
}

int main(void)
{
	Vector2 x2{1,2};
	Vector2 y2{2,1};
	Vector3 x3{1,2};
	Vector3 y3{2,1};
	cout << cosine(x2, y2) << endl;
	cout << cosine(x3, y3) << endl;
	//cout << cosine(3.3, 5.7) << endl; // --> compilation error
	isClockwise(x2,y2,x2,y2);
	//isClockwise(x3,y3,x3,y3); // --> compilation error
	isPlanar(x3,y3,x3,y3);
	//isPlanar(x2,y2,x2,y2); // --> compilation error
	cout << "hello world" << endl;
}


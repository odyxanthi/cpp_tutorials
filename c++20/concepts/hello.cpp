#include <iostream>
#include <concepts>
#include <map>
#include <unordered_map>


using namespace std;


template<typename T>
concept EqComparable = 
    requires(T a, T b) {
        { a == b };
        { a != b };
};

template<typename Map, typename K, typename V>
concept Mapping = 
	requires(Map<K,V> m){
		{ V v = m[K()] }
};

template <class T>
bool AreEqual(const T& a, const T& b) requires EqComparable<T>
{
	return a == b;
}

struct A
{
	int x;

	bool operator==(const A& rhs) const { cout << "A== called\n"; return x == rhs.x; }
	bool operator!=(const A& rhs) const { cout << "A!= called\n"; return x != rhs.x; }
};

struct B
{
	int x;

	bool operator==(const A& rhs) const = delete;
	bool operator!=(const A& rhs) const = delete;
};

template <class Key, class Val, Mapping<Key,Val> dict>
Val FindByKey(dict m, Key k)
{
	return m[k];	
}

int main(void)
{
	cout << AreEqual(1,2) << endl;
	cout << AreEqual(A{1},A{2}) << endl;
	cout << AreEqual(A{2},A{2}) << endl;
	//cout << AreEqual(B{1},B{2}) << endl;
	//cout << AreEqual(B{2},B{2}) << endl;
	
	std::map<int, string> x;
	std::unordered_map<int,string> y;
	auto x = FindByKey(x, 13);
	auto y = FindByKey(y, 17);

	cout << "hello world" << endl;
}	

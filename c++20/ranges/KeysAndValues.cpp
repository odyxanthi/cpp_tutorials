#include <map>
#include <ranges>
#include <iostream>
#include <string>

using namespace std;

int main()
{
	std::map<int,string> keyVals;
	for (int x : std::views::iota(2,11))
		keyVals[x] = std::to_string(x);

	for (const auto& k : keyVals | std::views::keys )
		std::cout << "key is " << k << endl;

	for (const auto& v : keyVals | std::views::values )
		std::cout << "value is " << ("_" + v + "_") << endl;

	auto vals = keyVals | std::views::keys
						| std::views::filter([](const auto& x){ return x%3==0 || x==7;})
						| std::views::transform([](const auto& v){ return "_"+std::to_string(v)+"^"; });
	for (const auto& v : vals)
		std::cout << v << endl;
	
	return 0;
}

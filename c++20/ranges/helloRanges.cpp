#include <vector>
#include <ranges>
#include <iostream>
 
using namespace std;

int main()
{
    std::vector<int> ints{0,1,2,3,4,5};
    auto even = [](int i){ 
		std::cout << "isEven:" << i << endl;
	    return 0 == i % 2; 
	};
    auto square = [](int i) { 
		std::cout << "square:" << i << endl;
		return i * i; 
	};

 
    for (int i : ints | std::views::filter(even) | std::views::transform(square)) {
        std::cout << "output " << i << ' ' << endl;
    }
}

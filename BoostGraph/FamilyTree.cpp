#include <iostream>
#include <fstream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/visitors.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/reverse_graph.hpp>

#include <map>
#include <vector>
#include <iostream>


using namespace boost;

typedef adjacency_list <vecS, vecS, bidirectionalS, property<vertex_name_t, std::string>, no_property > Graph;							//property < edge_name_t, std::string > > Graph;
	
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;


const std::string Stamatis_Sr	= "Stamatis_Sr";
const std::string Iraklis		= "Iraklis";
const std::string Marika		= "Marika";
const std::string Dimitris		= "Dimitris";
const std::string Elli			= "Elli";
const std::string Doris			= "Doris";
const std::string Stamatis_Jr	= "Stamatis_Jr";
const std::string Basw			= "Basw";
const std::string Giannis		= "Giannis";
const std::string Nikos			= "Nikos";
const std::string Elli_Jr		= "Elli_Jr";
const std::string Odysseas		= "Odysseas";
const std::string Dimitris_Jr	= "Dimitris_Jr";
const std::string Nefeli		= "Nefeli";
const std::string Dimitra		= "Dimitra";
const std::string Margarita		= "Margarita";
const std::string Katerina		= "Katerina";
const std::string Eleni			= "Eleni";


const std::vector<std::string> names{
	  Stamatis_Sr
	, Iraklis
	, Marika
	, Dimitris
	, Elli
	, Doris
	, Stamatis_Jr
	, Basw
	, Giannis
	, Nikos
	, Elli_Jr
	, Odysseas
	, Dimitris_Jr
	, Nefeli
	, Dimitra
	, Margarita
	, Katerina
	, Eleni
};


void CreateFamilyTree( Graph& g, std::map<const std::string, Vertex>& nameToVertexMap)
{
	typedef property_map<Graph, vertex_name_t>::type name_map_t;
	name_map_t getNameProp = get(vertex_name, g);

	for (const auto& name : names)
	{
		Vertex u = add_vertex(g);
		getNameProp[u] = name;
		nameToVertexMap[name] = u;
 	}


	auto addEdgeToGraph = [&nameToVertexMap](Graph& g,
											 const std::string& name1, 
											 const std::string& name2 )
											{
												Vertex u = nameToVertexMap[name1];
												Vertex v = nameToVertexMap[name2];
												add_edge(u, v, g);
											};

	addEdgeToGraph(g, Stamatis_Sr, Iraklis);
	addEdgeToGraph(g, Iraklis, Doris);
	addEdgeToGraph(g, Iraklis, Stamatis_Jr);
	addEdgeToGraph(g, Marika, Doris);
	addEdgeToGraph(g, Marika, Stamatis_Jr);

	addEdgeToGraph(g, Dimitris, Basw);
	addEdgeToGraph(g, Dimitris, Giannis);
	addEdgeToGraph(g, Dimitris, Nikos);
	addEdgeToGraph(g, Elli, Basw);
	addEdgeToGraph(g, Elli, Giannis);
	addEdgeToGraph(g, Elli, Nikos);

	addEdgeToGraph(g, Nikos, Elli_Jr);
	addEdgeToGraph(g, Doris, Elli_Jr);
	addEdgeToGraph(g, Nikos, Odysseas);
	addEdgeToGraph(g, Doris, Odysseas);

	addEdgeToGraph(g, Nikos, Dimitra);
	
	addEdgeToGraph(g, Giannis, Dimitris_Jr);

	addEdgeToGraph(g, Stamatis_Jr, Nefeli);
	addEdgeToGraph(g, Stamatis_Jr, Margarita);

	addEdgeToGraph(g, Eleni, Nefeli);
	addEdgeToGraph(g, Katerina, Margarita);
}


class BfsNodeTracker : public default_bfs_visitor
{
public:
	//BfsNodeTracker(){}

	BfsNodeTracker(std::vector<std::string>& nodesVisited)
		: m_nodesVisited(nodesVisited){}

	template <typename Edge, typename Graph>
	void tree_edge(Edge e, const Graph& g) const
	{
		typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
		Vertex u = source(e, g);
		Vertex v = target(e, g);
		
		typedef typename property_map<Graph, vertex_name_t>::const_type name_map_t;
		name_map_t getNameProp = get(vertex_name, g);

		//Graph& g_ = const_cast<Graph&>(g);
		//name_map_t getNameProp = get(vertex_name, g_);

		//typename property_map<Graph, vertex_name_t>::type getNameProp = get(vertex_name, g);
		m_nodesVisited.push_back(getNameProp[v]);
	}

private:
	std::vector<std::string>& m_nodesVisited;
};

template <class Graph>
std::vector<std::string> getSuccessors(const std::string& name, Graph& g, const std::map<const std::string, Vertex>& nameToVertexMap)
{
	std::vector<std::string> successors;

	auto iter = nameToVertexMap.find(name);
	if (iter != nameToVertexMap.end())
	{
		Vertex src = iter->second;

		BfsNodeTracker tracker(successors);
		breadth_first_search(g, src, visitor(tracker));
	}

	return successors;
}


//std::vector<std::string> getPredecessors(const std::string& name, Graph& g, const std::map<const std::string, Vertex>& nameToVertexMap)
//{
//	std::vector<std::string> predecessors;
//
//	auto iter = nameToVertexMap.find(name);
//	if (iter != nameToVertexMap.end())
//	{
//		Vertex src = iter->second;
//
//		BfsNodeTracker tracker(predecessors);
//		breadth_first_search(g, src, visitor(tracker));
//	}
//
//	return predecessors;
//}


int printPrevAndNext(void)
{
	Graph graph;
	std::map<const std::string, Vertex> nameToVertexMap;

	CreateFamilyTree(graph, nameToVertexMap);
	
	auto getNameProp = get(vertex_name, graph);

	std::string name;
	while (true)
	{
		std::cout << "type the name whose successors you want to know: ";
		std::cin >> name;

		auto iter = nameToVertexMap.find(name);
		if (iter != std::end(nameToVertexMap))
		{
			Vertex v = iter->second;
			Graph::out_edge_iterator out_begin, out_end;
			for (boost::tie(out_begin, out_end) = out_edges(v,graph); out_begin != out_end; ++out_begin)
			{   
				std::cout << getNameProp[target(*out_begin,graph)] << std::endl;
			}
			std::cout << std::endl;

			Graph::in_edge_iterator in_begin, in_end;
			for (boost::tie(in_begin, in_end) = in_edges(v,graph); in_begin != in_end; ++in_begin)
			{   
				std::cout << getNameProp[source(*in_begin,graph)] << std::endl;
			}
			std::cout << std::endl;
		}
		else
		{
			std::cout << "Name not found in the graph";
		}
	}

	return 0;
}


int printSuccessorsAndPredecessors(void)
{
	Graph g;
	std::map<const std::string, Vertex> nameToVertexMap;

	CreateFamilyTree(g, nameToVertexMap);

	boost::reverse_graph<Graph> rg(g);
	
	std::string name;
	while (true)
	{
		std::cout << "type the name whose successors you want to know: ";
		std::cin >> name;
		std::vector<std::string> successors = getSuccessors(name, g, nameToVertexMap);
		std::cout << "Found " << successors.size() <<" successors of " << name << "\n";
		for (auto& s : successors)
		{
			std::cout << "\t\t - " << s << "\n";
		}

		std::vector<std::string> predecessors = getSuccessors(name, rg, nameToVertexMap);
		std::cout << "Found " << predecessors.size() <<" predecessors of " << name << "\n";
		for (auto& s : predecessors)
		{
			std::cout << "\t\t - " << s << "\n";
		}

		if (name.empty())
		{
			break;
		}
	}

	return 0;
}


int main(void)
{
	printSuccessorsAndPredecessors();
	//printPrevAndNext();
	getchar();

	return 0;
}
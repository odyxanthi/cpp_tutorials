#include <iostream>
#include <map>
#include <string>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

using boost::range_detail::select_first_range;
using boost::range_detail::select_second_const_range;
using boost::range_detail::select_second_mutable_range;

class MyMap
{
public:
    typedef select_first_range<std::map<int, std::string> > KeyRange;
    typedef select_second_const_range<std::map<int, std::string> > ConstValueRange;
    typedef select_second_mutable_range<std::map<int, std::string> > NonConstValueRange;

    MyMap()
    {
        _map[1] = "_1";
        _map[2] = "_2";
        _map[3] = "_3";
        _map[4] = "_4";
        _map[11] = "_11";
        _map[22] = "_22";
        _map[33] = "_33";
        _map[44] = "_44";
    }
    
    KeyRange getKeys(void) const
    {
        std::cout << "KeyRange" << "\n";
        return boost::adaptors::keys(_map);
    }
    
    ConstValueRange getValues(void) const
    {
        std::cout << "constValueRange" << "\n";
        return boost::adaptors::values(_map);
    }
    
    
    NonConstValueRange getValues(void)
    {
        std::cout << "NonConstValueRange" << "\n";
        return boost::adaptors::values(_map);
    }
    
private:
    std::map<int, std::string> _map;
};

int main(void)
{
    std::cout << "hello world\n";
    
    MyMap m;
    for (auto& x : m.getValues())
    {
        std::cout << x << "\n";
    }
    
    const MyMap& m_const = m;
    for (auto& x : m_const.getValues())
    {
        std::cout << x << "\n";
    }
    
    for (auto& x : m.getKeys())
    {
        std::cout << x << "\n";
    }
    
    for (auto& x : m_const.getKeys())
    {
        std::cout << x << "\n";
    }
    
    return 0;
}
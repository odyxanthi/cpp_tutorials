#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <memory>
#include <utility>
//#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/range/adaptors.hpp>






class IntIterator : public boost::iterator_adaptor<IntIterator, std::vector<std::unique_ptr<int>>::iterator, int>
{
    typedef boost::iterator_adaptor<IntIterator, std::vector<std::unique_ptr<int>>::iterator, int> BaseIter; 
    
    public:
        explicit IntIterator(std::vector<std::unique_ptr<int>>::iterator it)
        : boost::iterator_adaptor<IntIterator, std::vector<std::unique_ptr<int>>::iterator, int>(it)
        {};
    
    private:
        friend class boost::iterator_core_access;
        
        int& dereference() const
        {
            return *(*(base_reference()));
        }
};



void IntPtrArray(void)
{
    std::vector<std::unique_ptr<int> > v;
	v.push_back(std::unique_ptr<int>(new int(1)));
	v.push_back(std::unique_ptr<int>(new int(7)));
	v.push_back(std::unique_ptr<int>(new int(3)));
	v.push_back(std::unique_ptr<int>(new int(2)));
	v.push_back(std::unique_ptr<int>(new int(5)));


    IntIterator begin(v.begin());
    IntIterator end(v.end());
    
    std::cout << "iterator adaptor tutorial\n";  
    for (auto i=begin;i!=end;++i)
    {
        std::cout << *i << "\n";
    }
}



struct Point
{
    double x;
    double y;
    
    double GetX() const { return x;}
    void SetX(double x) { this->x = x; }
};

std::ostream& operator << (std::ostream& out, const Point& p)
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}

template <class BaseIter>
class UniqueToRawPtrAdaptor : 
    public boost::iterator_adaptor< UniqueToRawPtrAdaptor<BaseIter>, 
                                    BaseIter, 
                                    typename BaseIter::value_type::element_type, 
                                    boost::forward_traversal_tag, 
                                    typename BaseIter::value_type::pointer>
{
    typedef typename boost::iterator_adaptor< UniqueToRawPtrAdaptor<BaseIter>, 
                                    BaseIter, 
                                    typename BaseIter::value_type::element_type, 
                                    boost::forward_traversal_tag, 
                                    typename BaseIter::value_type::pointer> BaseAdaptor;
    
public:
    UniqueToRawPtrAdaptor() : BaseAdaptor() {}
    explicit UniqueToRawPtrAdaptor(const BaseIter& iter) : BaseAdaptor(iter) {}
    
private:
    friend class boost::iterator_core_access;
    
    typename BaseIter::value_type::pointer dereference() const 
    { 
        return BaseAdaptor::base_reference()->get(); 
    }
};


void PointPtrArray(void)
{
    std::vector<std::unique_ptr<Point> > points;
    for (double i=0;i<9;++i)
        points.push_back( std::unique_ptr<Point>(new Point{i,i}) );
    
    typedef UniqueToRawPtrAdaptor<std::vector<std::unique_ptr<Point>>::iterator > PointPtrIter;
    typedef UniqueToRawPtrAdaptor<std::vector<std::unique_ptr<Point>>::const_iterator > ConstPointPtrIter;
    

    PointPtrIter begin(points.begin());
    PointPtrIter end(points.end());
    
    std::cout << "iterating a std::vector<std::unique_ptr<Point> > \n";  
    for (auto i=begin;i!=end;++i)
    {
        Point* p = *i;
        std::cout << *p << "\n";
    }
    

    const std::vector<std::unique_ptr<Point> >& const_points = points;
    ConstPointPtrIter cbegin(const_points.begin());
    ConstPointPtrIter cend(const_points.end());
    
    std::cout << "iterating a const std::vector<std::unique_ptr<Point> > \n";  
    for (auto i=cbegin;i!=cend;++i)
    {
        Point* p = *i;
        std::cout << *p << "\n";
        
    }

}


void PointPtrList(void)
{
    std::list<std::unique_ptr<Point> > points;
    points.push_back( std::unique_ptr<Point>(new Point{11,11}) );
    points.push_back( std::unique_ptr<Point>(new Point{22,22}) );
    points.push_back( std::unique_ptr<Point>(new Point{33,33}) );
    points.push_back( std::unique_ptr<Point>(new Point{44,44}) );
    points.push_back( std::unique_ptr<Point>(new Point{55,55}) );

    
    typedef UniqueToRawPtrAdaptor<std::list<std::unique_ptr<Point>>::iterator > PointPtrIter;
    typedef UniqueToRawPtrAdaptor<std::list<std::unique_ptr<Point>>::const_iterator > ConstPointPtrIter;
    

    PointPtrIter begin(points.begin());
    PointPtrIter end(points.end());
    
    std::cout << "iterating a std::list<std::unique_ptr<Point> > \n";  
    for (auto i=begin;i!=end;++i)
    {
        Point* p = *i;
        std::cout << *p << "\n";
    }
    

    const std::list<std::unique_ptr<Point> >& const_points = points;
    ConstPointPtrIter cbegin(const_points.begin());
    ConstPointPtrIter cend(const_points.end());
    
    std::cout << "iterating a const std::list<std::unique_ptr<Point> > \n";  
    for (auto i=cbegin;i!=cend;++i)
    {
        Point* p = *i;
        std::cout << *p << "\n";
    }
}


int main(void)
{
    IntPtrArray();
    PointPtrArray();
    PointPtrList();

    return 0;
}

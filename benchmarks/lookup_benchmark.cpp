#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>

using namespace std;

using KeyType = std::string;
using ValType = long;

template <class Key>
Key CreateKey(size_t){
    throw std::logic_error("CreateKey not implemented");
}

template <>
long CreateKey<long>(size_t k){
    return (long)k;
}

template <>
string CreateKey<string>(size_t k){
    return to_string(k);
}

template <class Key, class Val>
Val CreateValueFromKey(const Key& k){
    throw std::logic_error("CreateValueFromKey not implemented");
}

template <>
long CreateValueFromKey<long, long>(const long& key)
{
    long val;
    if (key%7 == 0)
        val = key*17;
    else if (key % 3 == 0)
        val = key*13;
    else if (key % 2 == 0)
        val = key*100;
    else
        val = key;
    return val;
}

template <>
long CreateValueFromKey<string, long>(const string& key)
{
    return stoi(key);
}


void Put(map<KeyType,ValType>& m, KeyType key, ValType val)
{
    m[key] = val;
}

void Put(unordered_map<KeyType,ValType>& m, KeyType key, ValType val)
{
    m[key] = val;
}

void Put(vector<pair<KeyType,ValType>>& m, KeyType key, ValType val)
{
    m.push_back({key,val});
}

void Put(list<pair<KeyType,ValType>>& m, KeyType key, ValType val)
{
    m.push_back({key,val});
}

ValType Get(const map<KeyType,ValType>& m, KeyType key)
{
    auto it = m.find(key);
    if (it!=m.end())
        return it->second;
    return -1;
}

ValType Get(const unordered_map<KeyType,ValType>& m, KeyType key)
{
    auto it = m.find(key);
    if (it!=m.end())
        return it->second;
    return -1;
}

ValType Get(const vector<pair<KeyType,ValType>>& m, KeyType key)
{
    for (auto& p : m)
        if (p.first==key)
            return p.second;
    return -1;
}

ValType Get(const list<pair<KeyType,ValType>>& m, KeyType key)
{
    for (auto& p : m)
        if (p.first==key)
            return p.second;
    return -1;
}

template<typename Container>
Container CreateIndex(size_t uniqueElementCount)
{
    Container container;
    for (size_t i=0; i < uniqueElementCount;++i)
    {
        auto key = CreateKey<KeyType>(i);
        Put(container, key, CreateValueFromKey<KeyType, ValType>(key));
    }
    return container;
}

template<typename Container>
vector<ValType> DoLookup(const vector<KeyType>& data, const Container& index)
{
    vector<ValType> result;
    result.reserve(data.size());
    for (auto& x : data)
        result.push_back(Get(index, x));
    return result;
}

vector<KeyType> CreateData(size_t uniqueElementCount, size_t totalElementCount)
{
    vector<KeyType> data;
    for (size_t i=0;i<totalElementCount;++i)
        data.push_back( CreateKey<KeyType>((rand() % uniqueElementCount)));
    return data;
}

struct BenchmarkSettings
{
    size_t uniqueElementCount = 0;
    size_t totalElementCount = 0;
};

int main(void)
{
    size_t numLookups = 10'000'000;
    const vector<BenchmarkSettings> settings {
        {     5, numLookups},
        {    10, numLookups},
        {   100, numLookups},
        { 1'000, numLookups},
        { 10'000, numLookups},
        { 100'000, numLookups},
        { 1'000'000, numLookups},
    };

    const size_t reps = 5;
    const char* c_vector="vector-lookup";
    const char* c_map="map-lookup";
    const char* c_unordMap="unord-map-lookup";

    for (const auto& setting : settings)
    {
        cout    << "Benchamark - unique elements: " << setting.uniqueElementCount 
                << ", number of lookups: " << setting.totalElementCount << endl;

        const auto vectorIndex   = CreateIndex<vector<pair<KeyType,ValType>>>(setting.uniqueElementCount);
        const auto mapIndex      = CreateIndex<map<KeyType,ValType>>(setting.uniqueElementCount);
        const auto unordMapIndex = CreateIndex<unordered_map<KeyType,ValType>>(setting.uniqueElementCount);

        map<string, double> overall;
        for (size_t rep=0; rep<reps;++rep)
        {
            auto runBenchmark = [](
                const char* benchmarkName, 
                const auto& data, 
                const auto& index,
                const auto& expectedResult) -> double
            {
                auto start = chrono::system_clock::now();
                auto result = DoLookup(data, index);
                auto end = chrono::system_clock::now();
                auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start).count()/1000.0;

                cout << benchmarkName << ", completed in " << elapsed 
                     << " sec, equals expected=" << (result == expectedResult) << endl;
                return elapsed;
            };

            auto data = CreateData(setting.uniqueElementCount, setting.totalElementCount);
            auto expectedResult = [&data](){
                vector<ValType> v;
                for (const auto& x : data) v.push_back(CreateValueFromKey<KeyType,ValType>(x));
                return v;
            }();

            overall[c_vector]   += runBenchmark(c_vector, data, vectorIndex, expectedResult);
            overall[c_map]      += runBenchmark(c_map, data, mapIndex, expectedResult);
            overall[c_unordMap] += runBenchmark(c_unordMap, data, unordMapIndex, expectedResult);
        }
        cout << "______________________________________________________________________" << endl;
        cout << "overall avg - " << c_vector    <<": " << overall[c_vector]/reps << endl;
        cout << "overall avg - " << c_map       <<": " << overall[c_map]/reps << endl;
        cout << "overall avg - " << c_unordMap  <<": " << overall[c_unordMap]/reps << endl;
        cout << "======================================================================\n\n" << endl;
    }
    return 0;
}
#include <cmath>
#include <iostream>



template <int N, int... Args>
struct ReverseBinary
{
	static int const value = (N == 0 ? 0 : 1) + 2 * ReverseBinary<Args...>::value;
};


template<int N>
struct ReverseBinary<N>
{
	static int const value  = (N == 0 ? 0 : 1);
};


template <int N, int... Args>
struct BinaryToInt
{
	static int digits(){ return 1 + BinaryToInt<Args...>::digits(); }
	static int value (){ return (N == 0 ? 0 : 1)*(int)std::pow(2, digits()-1) + BinaryToInt<Args...>::value(); }
};


template<int N>
struct BinaryToInt<N>
{
	static int digits(){ return 1; }
	static int value (){ return (N == 0) ? 0 : 1; }
};


int main(void)
{
	std::cout << "->  " << BinaryToInt<0, 0, 0>::value() << "\n";
	std::cout << "->  " << BinaryToInt<0, 0, 1>::value() << "\n";
	std::cout << "->  " << BinaryToInt<0, 1, 0>::value() << "\n";
	std::cout << "->  " << BinaryToInt<0, 1, 1>::value() << "\n";
	std::cout << "->  " << BinaryToInt<1, 0, 0>::value() << "\n";
	std::cout << "->  " << BinaryToInt<1, 0, 1>::value() << "\n";
	std::cout << "->  " << BinaryToInt<1, 1, 0>::value() << "\n";
	std::cout << "->  " << BinaryToInt<1, 1, 1>::value() << "\n";
	std::cout << "->  " << BinaryToInt<0, 0, 1, 0, 1, 1, 0>::value() << "\n";

	std::cout << "-----------------------------------------------\n";

	std::cout << "->  " << ReverseBinary<0, 0, 0>::value << "\n";
	std::cout << "->  " << ReverseBinary<0, 0, 1>::value << "\n";
	std::cout << "->  " << ReverseBinary<0, 1, 0>::value << "\n";
	std::cout << "->  " << ReverseBinary<0, 1, 1>::value << "\n";
	std::cout << "->  " << ReverseBinary<1, 0, 0>::value << "\n";
	std::cout << "->  " << ReverseBinary<1, 0, 1>::value << "\n";
	std::cout << "->  " << ReverseBinary<1, 1, 0>::value << "\n";
	std::cout << "->  " << ReverseBinary<1, 1, 1>::value << "\n";
	std::cout << "->  " << ReverseBinary<0, 0, 1, 0, 1, 1, 0>::value << "\n";

	std::cout << "-----------------------------------------------\n";

	return 0;
}


#if 0

template <int N, int... Rest>
struct max_t
{
	static int const value = max_t<Rest...>::value > N ? max_t<Rest...>::value : N;
};

template <int N>
struct max_t<N>
{
	static int const value = N;
};


template <int... NS>
int tmax()
{
	return max_t<NS...>::value;
}



template <typename... T>
class Num_Args;

template <>
struct Num_Args <>
{
	static size_t calculate() {
		return 0;
	}
};

template <typename H, typename... T>
struct Num_Args <H, T...>
{
	static size_t calculate() {
		return 1 + Num_Args<T...>::calculate();
	}
};

template <typename... T> B
size_t num_args()
{
	return Num_Args<T...>::calculate();
}


#endif
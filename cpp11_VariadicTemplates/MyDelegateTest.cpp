#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include "MyDelegate.h"

using namespace std;


double Divide2Numbers_Static(int x, int y)
{
	double res = x / (double)(y);
	cout << "Divide2Numbers_Static: " << res << "\n";
	return res;
}


class Calculator
{
public:
	Calculator(){};

public:

	double Divide2Numbers_Member(int x, int y)
	{
		double res = x / (double)(y);
		cout << "Divide2Numbers_Member: " << res << "\n";
		return res;
	}
};


int main(void)
{
	using DelegateType = IMyDelegate<double, int, int>;
	using DelegatePtr = std::unique_ptr<DelegateType>;
	
	// a list to store delegates
	std::vector<DelegatePtr> delegates;
	
	// register delegates
	Calculator c;
	delegates.push_back(std::make_unique<MyDelegate<Calculator, double, int, int>>(c, &Calculator::Divide2Numbers_Member));
	delegates.push_back(std::make_unique<MyDelegate<void, double, int, int>>(&Divide2Numbers_Static));
	
	// execute all delegates in the list.
	for (auto& d : delegates)
		d->Execute(5, 3);
	
	return 0;
}



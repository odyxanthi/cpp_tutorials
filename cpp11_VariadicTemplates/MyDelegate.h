#ifndef MYDELEGATE_H
#define MYDELEGATE_H



template <typename returnType, typename... ArgumentTypes >
class IMyDelegate
{
public:

	IMyDelegate(){};

public:

	virtual returnType Execute( ArgumentTypes... args ) = 0;

	returnType operator () (ArgumentTypes... args)	{ return this->Execute(args...); }
};




template <typename objType, typename returnType, typename... ArgumentTypes >
class MyDelegate : public IMyDelegate<returnType, ArgumentTypes... >
{
public:

	typedef returnType (objType::*funcType)(ArgumentTypes... args);

	MyDelegate(objType& obj_, funcType f)
		:
		IMyDelegate(),
		obj(obj_),
		func(f)
	{

	}


	virtual returnType Execute(ArgumentTypes... args)
	{
		return (obj.*func)(args...);
	};


private:
	objType& obj;
	funcType func;

};



// template specialization for objType = void
template < typename returnType, typename... ArgumentTypes>
class MyDelegate<void, returnType, ArgumentTypes...> : public IMyDelegate<returnType, ArgumentTypes... >
{
public:

	typedef returnType(*funcType)(ArgumentTypes... args);

	MyDelegate(funcType f)
		:
		IMyDelegate(),
		func(f)
	{

	}

	returnType Execute(ArgumentTypes... args)
	{
		return func(args...);
	};

private:

	funcType func;
};


#endif
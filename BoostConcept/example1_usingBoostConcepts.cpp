#include "stdafx.h"
#include <iostream>
#include <vector>
#include <list>
#include <type_traits>
#include <cmath>
#include "boost/concept_check.hpp"
#include "boost/concept/requires.hpp"
#include "boost/concept/assert.hpp"



template <class Rectangle>
double CalculateWidth(const Rectangle& rect)
{
	return std::abs(rect.right - rect.left);
}

template <class Rectangle>
double CalculateHeight(const Rectangle& rect)
{
	return std::abs(rect.top - rect.bottom);
}

template <class Rectangle>
double CalculateArea(const Rectangle& rect)
{
	return CalculateWidth(rect) * CalculateHeight(rect);
}

struct MyRect
{
	double width()
	{
		return right - left;
	}

	double Height()
	{
		return top - bottom;
	}

	double left;
	double right;
	double top;
	double bottom;
};


namespace StaticAssertApproach
{
	template <typename RangeOfPositiveInts>
	double AvgCupsOfCoffeePerPerson(const RangeOfPositiveInts& cupsOfCoffeePerPerson)
	{
		static_assert(std::is_same<unsigned int, RangeOfPositiveInts::value_type>::value, "expecting a range of 'unsigned int'");

		unsigned int sum = 0;
		for (auto it = std::begin(cupsOfCoffeePerPerson); it != std::end(cupsOfCoffeePerPerson); ++it)
		{
			sum += *it;
		}
		return sum / (double)(cupsOfCoffeePerPerson.size());
	}

	void RunTest()
	{
		std::cout << __FUNCTION__ << "\n";
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::vector<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::list<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "==============================================================\n\n\n";
	}
}


namespace BoostConceptAssertApproach
{
	template< typename RangeOfPositiveInts>
	double AvgCupsOfCoffeePerPerson( const RangeOfPositiveInts& container )
	{
		using namespace boost;

		// Check concepts and enforcement - NOTE THE REQUIRES EXTRA PARENTHESIS
		BOOST_CONCEPT_ASSERT( ( ContainerConcept< RangeOfPositiveInts > ) );               // Concept - container must actually be a container (e.g. has begin, end, iterators)
		BOOST_CONCEPT_ASSERT( ( UnsignedIntegerConcept< RangeOfPositiveInts::value_type > ) );       // Concept - container must hold unsigned integers
		BOOST_CONCEPT_ASSERT( ( InputIteratorConcept< RangeOfPositiveInts::const_iterator > ) );       // Concept - the container's const iterator must be an input iterator

		unsigned int sum = 0;
		for (auto it = std::begin(container); it != std::end(container); ++it)
		{
			sum += *it;
		}
		return sum / (double)(container.size());
	}

	void RunTest()
	{
		std::cout << __FUNCTION__ << "\n";
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::vector<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::list<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "==============================================================\n\n\n";
	}
}


namespace BoostConceptRequiresApproach
{
    // Function declaration
	template< typename RangeOfPositiveInts>
	BOOST_CONCEPT_REQUIRES(
		((ContainerConcept<RangeOfPositiveInts>))
		((UnsignedIntegerConcept<RangeOfPositiveInts::value_type>))
		((InputIteratorConcept<RangeOfPositiveInts::const_iterator>)),
		(double)
		)
		AvgCupsOfCoffeePerPerson(const RangeOfPositiveInts& container);

    // Function usage
	void RunTest()
	{
		std::cout << __FUNCTION__ << "\n";
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::vector<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "avg cups of coffee: " << AvgCupsOfCoffeePerPerson(std::list<unsigned int>{1, 2, 0, 1, 3, 4, 2}) << std::endl;
		std::cout << "==============================================================\n\n\n";
	}

    // Function definition
	template< typename RangeOfPositiveInts>
	AvgCupsOfCoffeePerPerson(const RangeOfPositiveInts& container)
	{
		unsigned int sum = 0;
		for (auto it = std::begin(container); it != std::end(container); ++it)
		{
			sum += *it;
		}
		return sum / (double)(container.size());
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	StaticAssertApproach::RunTest();
	BoostConceptAssertApproach::RunTest();
    BoostConceptRequiresApproach::RunTest();
	getchar();
	return 0;
}


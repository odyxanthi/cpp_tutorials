// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <list>
#include <iostream>
#include <type_traits>
#include <iterator>

#include "boost/concept_check.hpp"
#include "boost/concept/requires.hpp"

template <class P>
struct Point2DConcept
{
public:
	BOOST_CONCEPT_USAGE(Point2DConcept)
	{
		double x = p.GetX();
		double y = p.GetY();
	}

private:
	const P p;
};


template <class Point>
BOOST_CONCEPT_REQUIRES(
	((Point2DConcept<Point>)),
	(void)) 
CalculateCentroid(const std::vector<Point>& points, double& x, double& y)
{
	x = y = 0;
	for (const Point& p : points)
	{
		x += p.GetX();
		y += p.GetY();
	}
	x /= (double)points.size();
	y /= (double)points.size();
}


struct Point2D_A
{
	double x;
	double y;

	double GetX() const { return x; }
	double GetY() const { return y; }
};

struct Point2D_B
{
	double x;
	double y;

	double GetX_() const { return x; }
	double GetY_() const { return y; }
};

void Point2DConceptExample()
{
	std::vector<Point2D_A> va{ { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 } };
	std::vector<Point2D_B> vb{ { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 } };

	double a1, a2;
	CalculateCentroid(va, a1, a2);
	std::cout << a1 << ", " << a2 << "\n";

	//double b1, b2;
	//CalculateCentroid(vb, b1, b2);
	//std::cout << b1 << ", " << b2 << "\n";
}


template <class ListOfInts>
struct ListOfIntsConcept
{
public:
	typedef typename ListOfInts::const_iterator constIter;
	typedef typename ListOfInts::iterator iter;
	typedef typename ListOfInts::value_type value;

	//BOOST_CONCEPT_ASSERT((ForwardIterator<constIter>));
	//BOOST_CONCEPT_ASSERT((ForwardIterator<iter>));
	//BOOST_CONCEPT_ASSERT((Integer<value>));

	BOOST_CONCEPT_USAGE(ListOfIntsConcept)
	{
		static_assert(std::is_same<value, int>::value, "given container doesn't contain integers" );
		static_assert(std::is_base_of<std::bidirectional_iterator_tag, iter::iterator_category>::value, "given container doesn't support forward traversal");

		iter      begin  = list.begin();
		constIter cbegin = list.cbegin();
		iter      end    = list.end();
		constIter cend   = list.cend();
	}

	ListOfInts list;
};


template <class ListOfInts>
BOOST_CONCEPT_REQUIRES(
	((ListOfIntsConcept<ListOfInts>)),
	(double)
)
CalculateAverageAge(const ListOfInts& ages)
{
	double sum = 0.0;
	int count = 0;
	for (int age : ages)
	{
		sum += age;
		count++;
	}
	return sum / (double)count;
}


void ListOfIntsConceptExample()
{
	std::vector<int> l1{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::cout << "calculate avg age: " << CalculateAverageAge(l1) << "\n";
	
	std::list<int>   l2{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::cout << "calculate avg age: " << CalculateAverageAge(l2) << "\n";
		
	// The following does not compile. 
	// Uncomment to see the error given by the concept.
	//
	// std::vector<float> l3{ 1.2f, 2.3f, 3.4f };
	// std::cout << "calculate avg age: " << CalculateAverageAge(l3) << "\n";
}

int _tmain(int argc, _TCHAR* argv[])
{
	Point2DConceptExample();
	ListOfIntsConceptExample();
	getchar();
	return 0;
}


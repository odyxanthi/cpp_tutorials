#include <iostream>
#include <optional>
#include <ostream>
#include <vector>

using namespace std;


template <class Iter, class ValueGetter>
auto GetCommonValue(Iter begin, Iter end, ValueGetter vg)
       	-> optional<decltype(vg(*begin))>
{
	if (begin==end)
		return std::nullopt;
	else
	{
		auto val = vg(*(begin++));
		for (;begin!=end;++begin)
			if (vg(*begin) != val)
				return std::nullopt;
		return val;
	}
}


template <class T>
std::ostream& operator << (std::ostream& os, std::optional<T> opt)
{
	if (opt!= std::nullopt)
		os << *opt;
	else
		os << "<>";
	return os;
}


struct Point
{
	int x{0};
	int y{0};
};


int main(void)
{
	{
		std::vector<int> v1;
		std::vector<int> v2{32,1,1,1};
		std::vector<int> v3{67,67,67,67,67};
		using ValType = std::vector<int>::value_type;

		cout << "v1: " << GetCommonValue(v1.begin(), v1.end(), [](const ValType& v){ return v; }) << "\n";
		cout << "v2: " << GetCommonValue(v2.begin(), v2.end(), [](const ValType& v){ return v; }) << "\n";
		cout << "v3: " << GetCommonValue(v3.begin(), v3.end(), [](const ValType& v){ return v; }) << "\n";
	}

	{
		std::vector<Point> v1;
		std::vector<Point> v2{Point{1,2}, Point{3,2}, Point{4,2}};
		using ValType = std::vector<Point>::value_type;

		cout << "v1:   " << GetCommonValue(v1.begin(), v1.end(), [](const ValType& v){ return v.x; }) << "\n";
		cout << "v2.x: " << GetCommonValue(v2.begin(), v2.end(), [](const ValType& v){ return v.x; }) << "\n";
		cout << "v2.y: " << GetCommonValue(v2.begin(), v2.end(), [](const ValType& v){ return v.y; }) << "\n";
	}
	return 0;
}
	

#include <iostream>
#include <string>
#include <map>
#include <boost/any.hpp>
#include <boost/optional.hpp>


struct PersonalInfo
{
	std::string name;
	std::string surname;
};


struct Coordinates
{
	double latitude;
	double longitude;
};


class PropMap
{
public:
	void SetProperty(const std::string& name, boost::any value)
	{
		m_properties[name] = value;
	}

	boost::any GetProperty(const std::string& name) const
	{
		auto it = m_properties.find(name);
		if (it != m_properties.end())
			return it->second;
		else
			return boost::any();
	}

	void ClearAll()
	{
		m_properties.clear();
	}

private:

	std::map<std::string, boost::any> m_properties;
};


int main(void)
{
	PropMap propMap;
	propMap.SetProperty("id", PersonalInfo{ "James", "Bond" });
	propMap.SetProperty("location", Coordinates{ 90.034, 37.223 });

	PropMap map2{ propMap };
	propMap.ClearAll();

	auto prop = map2.GetProperty("id");
	if (prop.type() == typeid(PersonalInfo))
	{
		auto id = boost::any_cast<PersonalInfo>(prop);
		std::cout << "name:    " << id.name << "\n";
		std::cout << "surname: " << id.surname << "\n";
	}


	prop = map2.GetProperty("location");
	if (prop.type() == typeid(Coordinates))
	{
		auto loc = boost::any_cast<Coordinates>(prop);
		std::cout << "latitude:  " << loc.latitude << "\n";
		std::cout << "longitude: " << loc.longitude << "\n";
	}

	getchar();

	return 0;
}
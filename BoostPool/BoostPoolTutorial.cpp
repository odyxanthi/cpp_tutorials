// BoostPoolTutorial.cpp : Defines the entry point for the console application.
//

#include <boost/pool/object_pool.hpp>
#include <iostream>
#include <memory>
#include <vector>

class Point2D
{
public:
	Point2D(double x, double y)
		:x(x),y(y)
	{
		std::cout << "ctor: " << x << "," << y <<"\n";
	}

	~Point2D()
	{
		std::cout << "~dtor: " << x << "," << y << "\n";
	}

	double X()const { return x; }
	double Y()const { return y; }
private:
	double x;
	double y;
};


int main()
{
	boost::object_pool<Point2D> pool{ 4, 16 };
	auto deleter = [&pool](Point2D* p) { pool.destroy(p); };
	using PointUPtr = std::unique_ptr<Point2D, decltype(deleter)>;

	std::vector<PointUPtr> points;
	for (size_t i = 0; i < 100; ++i)
	{
		PointUPtr ptr(pool.construct((double)i, (double)(i * 10)), deleter);
		if (i % 3 == 0)
		{
			std::cout << "Discarding point " << ptr->X() << "," << ptr->Y() << "\n";
		}
		else
		{
			std::cout << "Storing    point " << ptr->X() << "," << ptr->Y() << "\n";
			points.push_back(std::move(ptr));
		}
	}

    return 0;
}


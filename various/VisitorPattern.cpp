// A basic example of applying visitor pattern to implement operations.
//
// Operations such as exporting to xml & json are implemented as visitors
// that provide appropriate overloads for handling different types of 
// objects. The benefit is that we don't need to modify the class on which
// the operation is defined which helps keep the class interface minimal;
// imagine the alternative of adding a method on each class for each new
// operation (e.g. Mesh::ExportWRL, Mesh::ExportSTL and so on). 
//

#include <iostream>
#include <vector>
#include <memory>

namespace Geometries
{
    class Mesh;
    class NURBSSurface;
    class NURBSCurve;
}

namespace Operations
{
    using namespace Geometries;

    // Visitor class : parent for various geometric operations.
    //
    // Issue 1): what happens if we have some other type of operations
    // e.g. database operations? Two options come to mind:
    // - Have two type of visitors: DatabaseObjVisitor & GeometryVisitor
    //   Geometry objects accept GeometryVisitor & database objects
    //   accept the DatabaseObjVisitor. If a class belongs to both 
    //   hierarchies should have one 'accept' method for each visitor.
    // - Have one visitor only, able to visit all type of objects.
    //   Then certainly there will visitor implementations representing
    //   operations well defined only for database objects or only for
    //   geometries. These visitors would have a meaningful implementation
    //   for the relevant classes, and throw an error for the rest.
    // Issue 2): if the class comes from a 3rd party library, it must
    // already implement the visitor pattern, in order to be able to visit
    // it.
    class Visitor
    {
    public:
        virtual void visit(Mesh&) = 0;
        virtual void visit(NURBSSurface&) = 0;
        virtual void visit(NURBSCurve&) = 0;
    };

    class XmlExporter : public Visitor
    {
    public:        
        virtual void visit(Mesh&) override { std::cout << "exporting mesh to XML\n"; }
        virtual void visit(NURBSSurface&) override {std::cout << "exporting Nurbs surface to XML\n";}
        virtual void visit(NURBSCurve&) override {std::cout << "exporting Nurbs curve to XML\n";}
    };

    class JsonExporter : public Visitor
    {
    public:
        virtual void visit(Mesh&) override { std::cout << "exporting mesh to JSON\n"; }
        virtual void visit(NURBSSurface&) override {std::cout << "exporting Nurbs surface to JSON\n";}
        virtual void visit(NURBSCurve&) override {std::cout << "exporting Nurbs curve to JSON\n";}
    };
}


namespace Geometries
{
    using Visitor = Operations::Visitor;

    class Geometry
    {
    public:
        virtual void accept(Visitor& v) = 0;
    };

    class Mesh : public Geometry
    {
    public:
        void accept(Visitor& v){ v.visit(*this); }
    };

    class NURBSSurface : public Geometry
    {
    public:
        void accept(Visitor& v){ v.visit(*this); }
    };

    class NURBSCurve : public Geometry
    {
    public:
        void accept(Visitor& v){ v.visit(*this); }
    };
}


int main(void)
{
    using namespace Operations;
    using namespace Geometries;

    XmlExporter xmlExporter;
    JsonExporter jsonExporter;

    using GeometryPtr = std::unique_ptr<Geometry>;
    std::vector<GeometryPtr> geometries;
    geometries.push_back(std::make_unique<Mesh>());
    geometries.push_back(std::make_unique<NURBSSurface>());
    geometries.push_back(std::make_unique<NURBSCurve>());

    for (GeometryPtr& g : geometries)
        g->accept(xmlExporter);
    for (GeometryPtr& g : geometries)
        g->accept(jsonExporter);
}
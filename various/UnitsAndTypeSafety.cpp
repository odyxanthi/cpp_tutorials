#include <iostream>
#include <cmath>

const double PI = 3.14159265359;

class AngleRad;
class AngleDeg;

struct Point2D
{
    Point2D(){};
    Point2D(double x, double y) : x(x), y(y){};

    double x=0;
    double y=0;
};

std::ostream& operator<<(std::ostream& os, const Point2D& p)
{
    os << "(" << p.x << ", " << p.y << ")";
}

class AngleRad
{
public:
    explicit AngleRad(double d);
    AngleRad(const AngleDeg& deg);
    explicit operator double () const { return val; }
private:
    double val=0;
};

class AngleDeg
{
public:
    explicit AngleDeg(double d);
    AngleDeg(const AngleRad& rad);
    explicit operator double () const { return val; }    
private:
    double val=0;
};

AngleRad::AngleRad(double d) : val(d){}
AngleRad::AngleRad(const AngleDeg& deg) : val(PI*(double)deg/180.0){}

AngleDeg::AngleDeg(double d) : val(d){}
AngleDeg::AngleDeg(const AngleRad& rad) : val(180.0*(double)rad/PI){}


Point2D RotatePoint(const Point2D& p, const AngleRad& rotationAngle)
{
    const double a = (double)rotationAngle;
    const double x = std::cos(a)*p.x - std::sin(a)*p.y;
    const double y = std::sin(a)*p.x + std::cos(a)*p.y;
    return Point2D{x,y};
}



int main(int argc, char const *argv[])
{
    std::cout << "hello world\n";

    // Calculations using radians
    std::cout << "Calculations using radians\n";
    std::cout << Point2D(1,0) << " rotated by PI/6 is " << RotatePoint(Point2D(1,0), AngleRad(PI/6.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by PI/4 is " << RotatePoint(Point2D(1,0), AngleRad(PI/4.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by PI/3 is " << RotatePoint(Point2D(1,0), AngleRad(PI/3.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by PI/2 is " << RotatePoint(Point2D(1,0), AngleRad(PI/2.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by PI   is " << RotatePoint(Point2D(1,0), AngleRad(PI    )) << "\n" ;

    std::cout << "Calculations using degrees\n";
    std::cout << Point2D(1,0) << " rotated by 30 is " << RotatePoint(Point2D(1,0), AngleDeg(30.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 45 is " << RotatePoint(Point2D(1,0), AngleDeg(45.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 60 is " << RotatePoint(Point2D(1,0), AngleDeg(60.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 90 is " << RotatePoint(Point2D(1,0), AngleDeg(90.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 180 is " << RotatePoint(Point2D(1,0), AngleDeg(180.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 225 is " << RotatePoint(Point2D(1,0), AngleDeg(225.0)) << "\n" ;
    std::cout << Point2D(1,0) << " rotated by 270 is " << RotatePoint(Point2D(1,0), AngleDeg(270.0)) << "\n" ;

    return 0;
}

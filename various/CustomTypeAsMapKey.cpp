#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <utility>
#include <rpc.h>


template <class T>
std::wstring ToString(const T& t)
{
	std::wstringstream ss;
	ss << "\"" << t << "\"";
	return ss.str();
}


template <>
std::wstring ToString(const GUID& t)
{
	OLECHAR* bstrGuid;
	StringFromCLSID(t, &bstrGuid);
	std::wstring ws(bstrGuid);

	std::wstringstream ss;
	ss << "\"" << ws << "\"";

	::CoTaskMemFree(bstrGuid);

	return ss.str();
}


template <class T, class TComparer = std::less<T> >
class Registry
{
public:
	void RegisterElement(const T& t, int value)
	{
		m_map[t] = value;
	}

	void PrintRegisteredValues()
	{
		for (const std::pair<T, int>& p : m_map)
		{
			std::wcout << ToString(p.first) << "-->" << p.second << "\n";
		}
	}

private:

	std::map<T, int, TComparer> m_map;
	//std::map<T, int> m_map;
	//std::vector<std::pair<T, int>> m_map;
};



void TestInts()
{
	Registry<int> reg;
	reg.RegisterElement(1, 11);
	reg.RegisterElement(2, 12);
	reg.RegisterElement(3, 13);
	reg.RegisterElement(4, 14);
	reg.RegisterElement(5, 15);

	reg.PrintRegisteredValues();
}


namespace test1{
	namespace abc
	{
		struct Comparer
		{
			bool operator()(const GUID& id1, const GUID& id2) const
			{
				//return memcmp(&id1 , &id2,sizeof(id2)) < 0;
				std::wstring s1 = ToString(id1);
				std::wstring s2 = ToString(id2);

				return (s1 < s2);
			}
		};
	}

	void TestGUIDs()
	{
		Registry<GUID, abc::Comparer> reg;

		GUID guid;

		for (int i = 0; i < 30;++i)
		{
			CoCreateGuid(&guid);
			reg.RegisterElement(guid, i);
		}
	
		reg.PrintRegisteredValues();
	}
}



bool operator < (const GUID& id1, const GUID& id2)
{
	// TODO: How can I make test2 compile without having to put operator < in the global namespace???
	//return memcmp(&id1 , &id2,sizeof(id2)) < 0;
	std::wstring s1 = ToString(id1);
	std::wstring s2 = ToString(id2);

	return (s1 < s2);
}

namespace test2
{



	void TestGUIDs_2()
	{
		Registry<GUID> reg;

		GUID guid;

		for (int i = 0; i < 30;++i)
		{
			CoCreateGuid(&guid);
			reg.RegisterElement(guid, i);
		}
	
		reg.PrintRegisteredValues();
	}
}





int main(void)
{
	std::cout << "hellow world\n";

	TestInts();
	std::cout << "------------------------------------------------------------------------\n";
	test1::TestGUIDs();
	std::cout << "------------------------------------------------------------------------\n";
	test2::TestGUIDs_2();

	getchar();
	return 0;
}
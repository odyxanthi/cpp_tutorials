#include "MyString.h"

#include <utility>
#include <string.h>
#include <exception>
#include <memory>
#include <iostream>


using namespace std;


MyString::MyString()
: str("\0")
{
	cout << "defauult constructor called\n";
}


MyString::MyString(const MyString& otherStr)
: str(new char[otherStr.Length()+1])
{
	cout << "copy constructor called\n";

	strcpy(this->str, otherStr.c_str());
}


MyString::MyString(MyString&& otherStr)
{
	cout << "move constructor called\n";

	std::swap(this->str, otherStr.str);
}



MyString::MyString(const char* otherStr)
: str(nullptr)
{
	cout << "constructor(char*) called\n";

	if (otherStr == nullptr)	{ throw std::exception();}

	this->str = new char[strlen(otherStr) + 1];
	strcpy(this->str, otherStr);
}



MyString& MyString::operator = (MyString otherStr)
{
	cout << "assignement operator called\n";

	std::swap(this->str, otherStr.str);

	return (*this);
}

//MyString& operator = (const char* otherStr);



//MyString MyString::operator + (const MyString& str1)
//{
//	cout << "operator+ called\n";
//
//	char* charBuf = new char[this->Length() + str1.Length() + 1];
//
//	strcpy(charBuf, this->c_str());
//	strcpy(&charBuf[this->Length()], str1.c_str());
//
//	MyString s(charBuf);
//
//	delete charBuf;
//
//	return s;
//}


MyString MyString::operator + (const MyString& str1)
{
	cout << "operator+ called\n";

	MyString myCopy(*this);
	myCopy += str1;

	return myCopy;
}


MyString& MyString::operator += (const MyString& otherStr)
{
	cout << "operator+= called\n";

	char* newBuf = new char[this->Length() + otherStr.Length() + 1];
	strcpy(newBuf, this->c_str());
	strcpy(&newBuf[this->Length()], otherStr.c_str());

	delete this->str;
	this->str = newBuf;

	return (*this);
}


const char* MyString::c_str(void) const
{
	return (this->str);
}

size_t MyString::Length(void) const
{
	return strlen(this->str);
}





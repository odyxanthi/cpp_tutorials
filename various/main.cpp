#include <iostream>
#include <stdio.h>

#include "MyString.h"
#include "MyDelegate.h"



using namespace std;


void FilterCollectionElementsExample(int threshold);
void RunDelegateExample_01(void);
void RunDelegateExample_02(void);


void MyStringTest1(void)
{
	cout << "MyStringTest1 started \n";

	MyString s1;
	MyString s2("123");

	MyString s3(s1 + s2);

	cout << "s3 = " << s3.c_str() << "\n";

	cout << "MyStringTest1 ended \n";
}



int main_prev(void)
{
	//cout << "Hello world\n";

	//MyStringTest1();

	//RunDelegateExample_01();

	//FilterCollectionElementsExample(12);

	char c = getchar();

	return 0;
}
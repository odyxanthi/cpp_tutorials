#include <vector>
#include <list>
#include <functional>
#include <tuple>
#include <string>
#include <iostream>

using namespace std;


//template <typename CollectionType, typename AppliedFunc, typename FilteringFunc, typename ResultType>
//vector<ResultType> FilterCollectionElements(const CollectionType& collection, AppliedFunc& applied_f, FilteringFunc& filter_f)
//{
//	for (auto iter : collection.cbegin())
//
//}



vector<tuple<double,double,bool>> FilterCollectionElements(list<double>& collection, std::function<double(double)>& applied_f, std::function<bool(double)>& filter_f)
{
	vector<tuple<double,double,bool>> v;

	for (auto iter : collection)
	{
		double d = applied_f(iter);

		if (filter_f(d) == true)
		{
			v.push_back(tuple<double,double,bool>(iter, d,filter_f(d) ) );
		}
	}

	return v;
}




void FilterCollectionElementsExample( int threshold )
{
	std::list<double> l = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	std::function<double(double)> f1 = [](double x){ return x*x;	 };
	std::function<bool(double)> f2 = [threshold](double x){ return (x > threshold);	 };

	std::vector < tuple<double, double, bool> > v = FilterCollectionElements(l, f1, f2);

	for (auto t : v)
	{
		cout << std::get<0>(t) << "," << std::get<1>(t) << "," << std::get<2>(t) << "\n";
	}
}
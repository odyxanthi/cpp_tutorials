
#include <iostream>
#include <list>
#include <vector>
#include <array>

//
// myMathLib : my math library - defines a vector class
//
namespace myMathLib
{
	//
	// Class Vector<T,N> : N-dimensional vector with each component being of type T.
	//
	template <typename T, size_t Length>
	class Vector
	{
	public:
		Vector(){}

		Vector(const std::array<T, Length>& data)
			: data(data) {}

		Vector(const Vector<T, Length>& v)
			: data(v.data){}

		bool operator == (const Vector<T, Length>& v)
		{
			for (int i = 0; i < Length; i++)
				if (data[i] != v.data[i])
					return false;
			return true;
		}

		T & operator [] (size_t index)
		{
			return data[index];
		}

		const T& operator [] (size_t index) const
		{
			return data[index];
		}

		Vector<T, Length>&	operator =	(Vector<T, Length> v)        // copy and swap
		{
			data = v.data;
			return *this;
		}

		Vector<T, Length>	operator +	(const Vector<T, Length>& v) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] + v.data[i];
			}
			return resultV;
		}

		Vector<T, Length>&	operator += (const Vector<T, Length>& v)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] += v.data[i];
			}
			return *this;
		}

		Vector<T, Length>	operator -	(const Vector<T, Length>& v) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] - v.data[i];
			}
			return resultV;
		}

		Vector<T, Length>&	operator -= (const Vector<T, Length>& v)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] -= v.data[i];
			}
			return *this;
		}

		Vector<T, Length>	operator *	(const T& t) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] * t;
			}
			return resultV;
		}

		Vector<T, Length>&	operator *= (const T& t)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] *= t;
			}
			return *this;
		}

		Vector<T, Length>	operator /	(const T& t) const
		{
			Vector<T, Length> resultV;
			for (size_t i = 0; i<Length; i++)
			{
				resultV.data[i] = this->data[i] / t;
			}
			return resultV;
		}

		Vector<T, Length>&	operator /= (const T& t)
		{
			for (size_t i = 0; i<Length; i++)
			{
				this->data[i] /= t;
			}
			return *this;
		}

		T DotProduct(const Vector<T, Length>& v) const
		{
			T t = 0;
			for (size_t i = 0; i<Length; i++)
			{
				t += this->data[i] * v.data[i];
			}
			return t;
		}

	protected:
		std::array<T, Length> data;
	};

	//
	// Class Vector<T,3> : Specialization for 3-dimensonal vector
	//
	template <typename T>
	class Vector3 : public Vector<T, 3>
	{
	public:
		Vector3() = default;

		Vector3(const T& x, const T& y, const T& z)
			:
			Vector<T, 3>(std::array<T, 3>{x, y, z})
		{}


		Vector3(const Vector<T, 3>& v)
			:
			Vector<T, 3>(v)
		{}

	public:

		inline T& X(void)
		{
			return this->data[0];
		}


		inline const T& X(void) const
		{
			return this->data[0];
		}


		inline T& Y(void)
		{
			return this->data[1];
		}


		inline const T& Y(void) const
		{
			return this->data[1];
		}


		inline T& Z(void)
		{
			return this->data[2];
			//return this->operator[](2);
		}


		inline const T& Z(void) const
		{
			return this->data[2];
		}

	public:

		Vector3<T> CrossProduct(const Vector3<T>& vec) const
		{
			return Vector3<T>(Y() * vec.Z() - Z() * vec.Y(),
				Z() * vec.X() - X() * vec.Z(),
				X() * vec.Y() - Y() * vec.X());
		}

	};

	//
	// Class Vector<T,2> : Specialization for 2-dimensonal vector
	//
	template <typename T>
	class Vector2 : public Vector<T, 2>
	{
	public:
		Vector2() = default;


		Vector2(const T& x, const T& y)
			:
			Vector<T, 2>(std::array<T, 2>{x, y})
		{}


		Vector2(const Vector<T, 2>& v)
			:
			Vector<T, 2>(v)
		{}

	public:

		inline T& X(void)
		{
			return this->data[0];
		}


		inline const T& X(void) const
		{
			return this->data[0];
		}


		inline T& Y(void)
		{
			return this->data[1];
		}


		inline const T& Y(void) const
		{
			return this->data[1];
		}
	};

	template <typename T, size_t Length >
	std::ostream& operator << (std::ostream& s, const Vector<T, Length>& v)
	{
		if (Length > 0)
			s << "(" << v[0];
		for (size_t i = 1; i < Length; i++)
			s << "," << v[i];
		s << ")";

		return s;
	}

	//
	// Aliases for commonly used classes.
	//
	typedef Vector2<double> vector2d;
	typedef Vector2<float>  vector2f;

	typedef Vector3<double> vector3d;
	typedef Vector3<float>  vector3f;

	typedef Vector<double, 4> vector4d;
	typedef Vector<float, 4> vector4f;
}

//
// 3rd party lib : defines Point3D class which is pretty similar with the vector
// class defined in my library, however it doesn't have the same interface.
//
namespace thirdPartyLib
{
	class Point3D
	{
	public:
		Point3D() {}
		Point3D(double x, double y, double z) : x(x), y(y), z(z) {};

		double& X() { return x; }
		double& Y() { return y; }
		double& Z() { return z; }

		double  X() const { return x; }
		double  Y() const { return y; }
		double  Z() const { return z; }

	private:
		double x{ 0 }, y{ 0 }, z{ 0 };
	};
}

//
// myGeometryLib : implementation of common geometric operations.
//
namespace myGeometryLib
{
	template <class Range>
	auto CalcCentroid(const Range& points) -> typename Range::value_type
	{
		if (points.size() == 0)
			throw std::invalid_argument("range must contain at least one element");
		typename Range::value_type v{ 0,0,0 };
		for (auto& p : points)
			v += p;
		return v / points.size();
	}
}

//
// thirdPartyLib : Adapters to allow 3rd party class be used with 
//				   the algorithms in myGeometryLib. 
//
namespace thirdPartyLib
{
	//
	// Implement operators required by algorithms in myGeometryLib.
	// Since they are in the same namespace with Point3D, they will
	// be discovered by argument dependent lookup.
	//

	Point3D& operator	+=	(Point3D& v1, const Point3D& v2)
	{
		v1.X() += v2.X();
		v1.Y() += v2.Y();
		v1.Z() += v2.Z();
		return v1;
	}

	Point3D operator	+	(const Point3D& v1, const Point3D& v2)
	{
		Point3D v{ v1 };
		v += v2;
		return v;
	}

	Point3D& operator	-=	(Point3D& v1, const Point3D& v2)
	{
		v1.X() -= v2.X();
		v1.Y() -= v2.Y();
		v1.Z() -= v2.Z();
		return v1;
	}

	Point3D operator	-	(const Point3D& v1, const Point3D& v2)
	{
		Point3D v{ v1 };
		v -= v2;
		return v;
	}

	Point3D& operator	*=	(Point3D& v1, const double d)
	{
		v1.X() *= d;
		v1.Y() *= d;
		v1.Z() *= d;
		return v1;
	}

	Point3D operator	*	(const Point3D& v1, const double d)
	{
		Point3D v{ v1 };
		v *= d;
		return v;
	}

	Point3D& operator	/=	(Point3D& v1, const double d)
	{
		v1.X() /= d;
		v1.Y() /= d;
		v1.Z() /= d;
		return v1;
	}

	Point3D operator	/	(const Point3D& v1, const double d)
	{
		Point3D v{ v1 };
		v /= d;
		return v;
	}

	std::ostream& operator << (std::ostream& s, const Point3D& v)
	{
		s << "(" << v.X();
		s << "," << v.Y();
		s << "," << v.Z();
		s << ")";
		return s;
	}
}



int main()
{
	using vector3d = myMathLib::vector3d;
	using Point3D = thirdPartyLib::Point3D;
	using namespace myGeometryLib;

	std::vector<vector3d> v1{
		vector3d{0,0,0},
		vector3d{1,1,0},
		vector3d{0,1,0}
	};

	std::vector<Point3D> v2{
		Point3D{ 0,0,0 },
		Point3D{ 10,10,0 },
		Point3D{ 0,10,0 }
	};

	std::list<Point3D> v3{
		Point3D{ 0,0,0 },
		Point3D{ 100,100,0 },
		Point3D{ 0,100,0 }
	};

	std::cout << "centroid = " << CalcCentroid(v1) << "\n";
	std::cout << "centroid = " << CalcCentroid(v2) << "\n";
	std::cout << "centroid = " << CalcCentroid(v3) << "\n";

    return 0;
}


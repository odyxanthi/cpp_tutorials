#ifndef MYSTRING_H
#define MYSTRING_H



class MyString
{
public:

	MyString();

	MyString(const MyString& otherStr);

	MyString(MyString&& otherStr);

	MyString(const char* otherStr);

public:

	MyString& operator = ( MyString otherStr);

	//MyString& operator = (const char* otherStr);

public:

	MyString operator + (const MyString& str1);

	//MyString operator + (MyString str1 );

	MyString& operator += (const MyString& otherStr);

public:

	size_t Length(void) const;


public:

	const char* c_str(void) const;

private:

	//static void swap(MyString& str1, MyString& str2);

private:

	char* str;
	//size_t length;
};



#endif
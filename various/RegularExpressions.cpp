#include <iostream>
#include <regex>
#include <string>

class CGeneralFileUtils
{
   public:

    static bool IsValidRelativeDirectory(const std::string& s)
    {
        const std::string leadingSlash	= "(\\\\)?";						// optional leading slash
        const std::string folderName	= "[^*\"<>:|/\?\\\\]+";				// folderName: sequence that doesn't contain *"<>:|/?\ 
        const std::string subfolders	= std::string("(\\\\") + folderName + std::string(")*");		// \\folderName repeated zero or more times.
        const std::string trailingSlash	= "(\\\\)?";						// optional trailing slash.
        std::regex validPathRegex  (leadingSlash+folderName+subfolders+trailingSlash);

        return std::regex_match(s, validPathRegex);
    }

    static bool IsValidRelativeDirectory(const std::wstring& s)
    {
        const std::wstring leadingSlash	= L"(\\\\)?";						// optional leading slash
        const std::wstring folderName	= L"[^*\"<>:|/\?\\\\]+";				// folderName: sequence that doesn't contain *"<>:|/?\ 
        const std::wstring subfolders	= std::wstring(L"(\\\\") + folderName + std::wstring(L")*");		// \\folderName repeated zero or more times.
        const std::wstring trailingSlash = L"(\\\\)?";						// optional trailing slash.
        std::basic_regex<wchar_t> validPathRegex  (leadingSlash+folderName+subfolders+trailingSlash);

        return std::regex_match(s, validPathRegex);
    }
};
        



void FolderNameTests()
{
	// Valid characters
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc123") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc123") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc123_ # $ % � !  � $ % ^ &  ( ) - + = [ ] { } @ ' # ~") << "\n"; // a case containing various uncommon but allowed characters.
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc123_ # $ % � !  � $ % ^ &  ( ) - + = [ ] { } @ ' # ~") << "\n"; // a case containing various uncommon but allowed characters.

	// Invalid characters
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e/GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e/GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e*GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e*GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e\"GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e\"GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e<GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e<GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e>GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e>GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e|GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e|GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e?GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e?GH") << "\n";

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory("abc23e:GH") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc23e:GH") << "\n";
}


void PathsMoreThanOneLevelDeep()
{
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(abc)") << "\n";					// abc
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(\abc)") << "\n";				// \abc
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(abc\def)") << "\n";				// abc\def
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(\abc\def)") << "\n";			// \abc\def
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(abc\def\)") << "\n";			// abc\def\ 
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(\abc\def\)") << "\n";			// \abc\def\ 
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(\\abc)") << "\n";				// \\abc

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(.)") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(..)") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(.\)") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(..\)") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(.\..)") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(.\a...b)") << "\n";

	// ... shouldn't be allowed
	//std::cout << CGeneralFileUtils::IsValidRelativeDirectory(R"(...)") << "\n";


	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc") << "\n";					// abc
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"\\abc") << "\n";					// \abc
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc\\def") << "\n";				// abc\def
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"\\abc\\def") << "\n";			// \abc\def
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"abc\\def\\") << "\n";			// abc\def\ 
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"\\abc\\def\\") << "\n";			// \abc\def\ 
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"\\\\abc") << "\n";				// \\abc

	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L".") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"..") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L".\\") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L"..\\") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L".\\..") << "\n";
	std::cout << CGeneralFileUtils::IsValidRelativeDirectory(L".\\a...b") << "\n";
}


int main(void)
{
    FolderNameTests();
    PathsMoreThanOneLevelDeep();
}
#include <array>
#include <iostream>
#include <memory>
#include <thread>
#include <future>
#include <vector>
#include <fstream>

template <size_t M, size_t N>
using Matrix = std::array<std::array<int, N>, M>;


template<size_t M, size_t N>
std::ostream& operator<<(std::ostream& os, const Matrix<M, N>& m)
{
	for (auto i = 0u; i < M; ++i)
	{
		for (auto j = 0u; j < N; ++j)
		{
			os << m[i][j] << ",";
		}
		os << "\n";
	}
	return os;
}


template <size_t M, size_t N, size_t K>
auto operator * (
	const Matrix<M,N>& A,
	const Matrix<N,K>& B)
	-> std::unique_ptr<Matrix<M,K>>
{
	auto m = std::make_unique<Matrix<M, K>>();
	for (auto i = 0u; i < M; ++i)
	{
		for (auto k = 0u; k < K; ++k)
		{
			(*m)[i][k] = 0;
			for (auto n = 0u; n < N; ++n)
			{
				(*m)[i][k] += A[i][n] * B[n][k];
			}
		}
	}
	return m;
}



template <size_t M, size_t N, size_t K>
auto multParallel (
	const Matrix<M, N>& A,
	const Matrix<N, K>& B)
	-> std::unique_ptr<Matrix<M, K>>
{
	auto m = std::make_unique<Matrix<M, K>>();

	// Multiplies rows l,l+1,...h-1 from A, with all columns of B.
	// i.e. calculates rows l,l+1,...h-1 of the result matrix.
	auto calcRows = [&A, &B, &m](const auto l, const auto h)
	{
		for (auto i=l;i<h;++i)
			for (auto k = 0u; k < K; ++k)
			{
				(*m)[i][k] = 0;
				for (auto n = 0u; n < N; ++n)
				{
					(*m)[i][k] += A[i][n] * B[n][k];
				}
			}
	};

	std::vector<decltype(std::async(calcRows, 0, 5))> futures;
	const auto numThreads = 8u;
	size_t step = M / numThreads;
	for (auto i = 0u; i < M; i+= step)
		futures.push_back(std::async(std::launch::async, calcRows, i, std::min(M, i+step)));
	for (auto& f : futures)
		f.get();
	return m;
}


class Counter
{
public:

	int operator()()
	{
		c = (c + 1) % 20;
		return std::abs(10 - c);
	}

private:
	int c{ 0 };
};


int main()
{
	Counter c;
	const size_t M{ 1000 }, N{ 2000 }, K{6000};
	auto pA = std::make_unique<Matrix<M, N>>();
	for (auto i = 0u; i < M; ++i)
		for (auto j = 0u; j < N; ++j)
			(*pA)[i][j] = c();

	auto pB = std::make_unique<Matrix<N, K>>();
	for (auto i = 0u; i < N; ++i)
		for (auto j = 0u; j < K; ++j)
			(*pB)[i][j] = c();


	//std::cout << (*pA) << "\n";
	//std::cout << (*pB) << "\n";

	{
		auto start = std::chrono::steady_clock::now();
		auto pC = (*pA)*(*pB);
		auto end = std::chrono::steady_clock::now();
		auto diff = end - start;

		std::cout << "calc time: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << "\n\n\n\n";
		std::cout.flush();

		//std::fstream f("serial.txt", std::ios::out);
		//f << "calc time: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << "\n\n\n\n";
		//f << (*pC) << "";
		
	}

	{
		auto start = std::chrono::steady_clock::now();
		auto pC = multParallel(*pA, *pB);
		auto end = std::chrono::steady_clock::now();
		auto diff = end - start;
		
		std::cout << "calc time: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << "\n\n\n\n";
		std::cout.flush();


		//std::fstream f("multi.txt", std::ios::out);
		//f << "calc time: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << "\n\n\n\n";
		//f << (*pC) << "";
		
	}

    return 0;
}

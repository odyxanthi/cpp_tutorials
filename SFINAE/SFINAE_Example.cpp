// SFINAE_Example.cpp

#include <iostream>
#include <list>
#include <vector>


template <
	  class Range
	, std::enable_if_t<std::is_same<typename Range::iterator::iterator_category, std::random_access_iterator_tag>::value, int>* = nullptr
>
auto GetElementAtIdx(const Range& r, size_t idx) -> typename Range::value_type
{
	if (idx >= r.size())
		throw std::logic_error("idx out of bounds"); 
	return *(r.begin() + idx);
}


template <
	  class Range
	, std::enable_if_t<!std::is_same<typename Range::iterator::iterator_category, std::random_access_iterator_tag>::value, int>* = nullptr
> 
auto GetElementAtIdx(const Range& r, size_t idx) -> typename Range::value_type
{
	if (idx >= r.size())
		throw std::logic_error("idx out of bounds");
	size_t count = 0;
	auto it = r.begin();
	for (; count < idx; ++count, ++it)
	{
		if (count == idx)	break;
	}
	return *it;
}



int main()
{
	std::vector<int> v1{  1,2,3,4,5,6,7,8,9 };
	std::list<int>   l1{ 11,12,13,14,15,16,17,18,19 };

	std::cout << "v1 allows random access: " << std::is_same<std::vector<int>::iterator::iterator_category, std::random_access_iterator_tag>::value << "\n";
	std::cout << "l1 allows random access: " << std::is_same<std::list<int>::iterator::iterator_category, std::random_access_iterator_tag>::value << "\n";

	std::cout << "v1[0] = " << GetElementAtIdx(v1, 0) << "\n";
	std::cout << "v1[3] = " << GetElementAtIdx(v1, 3) << "\n";
	std::cout << "v1[8] = " << GetElementAtIdx(v1, 8) << "\n";
	
	std::cout << "l1[0] = " << GetElementAtIdx(l1, 0) << "\n";
	std::cout << "l1[3] = " << GetElementAtIdx(l1, 3) << "\n"; 
	std::cout << "l1[8] = " << GetElementAtIdx(l1, 8) << "\n";

    return 0;
}


#include <iostream>

using namespace std;

class A
{
public:
    double GetSymbol()
    {
        cout << "A::GetSymbol" << endl;
	return 0.0;
    }

    void GetFixTag(int x)
    {
        cout << "A::GetFixTag " << x << endl;
    }
};

class B
{
public:
    int GetSymbol()
    {
        cout << "B::GetSymbol" << endl;
	return 0;
    }

    void GetPrice()
    {
        cout << "B::GetPrice" << endl;
    }

    void GetFixTag(int x)
    {
        cout << "B::GetFixTag " << x << endl;
    }
};

class C
{
public:
    void GetFixTag(int x)
    {
        cout << "C::GetFixTag " << x << endl;
    }

    void GetPrice()
    {
        cout << "C::GetPrice" << endl;
    }
};

template<int Tag>
struct TagGetter
{
    template <class Obj> 
	void get(Obj& o){ o.GetFixTag(Tag); }
};

#define define_has_member(member_name)                                         \
    template <typename T>                                                      \
    class has_member_##member_name                                             \
    {                                                                          \
        typedef char yes_type;                                                 \
        typedef long no_type;                                                  \
        template <typename U> static yes_type test(decltype(&U::member_name)); \
        template <typename U> static no_type  test(...);                       \
    public:                                                                    \
        static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes_type);  \
    };                                                                       

#define has_member(class_, member_name)  has_member_##member_name<class_>::value

#define field_getter(tag_num, func_name)                                       \
define_has_member(func_name)                                                   \
template<>                                                                     \
struct TagGetter<tag_num>                                                      \
{                                                                              \
    template <class Obj                                                        \
              , std::enable_if_t< has_member(Obj, func_name) , bool > = true>  \
    void get(Obj& o){ o.func_name(); }                                         \
                                                                               \
    template <class Obj                                                        \
              , std::enable_if_t< !has_member(Obj, func_name) , bool > = true> \
    void get(Obj& o){ o.GetFixTag(tag_num); }                                  \
};



field_getter(1, GetSymbol)
field_getter(2, GetPrice)

int main(void)
{
    cout << "hello world\n";
    A a; B b; C c;
    TagGetter<1>().get(a);
    TagGetter<2>().get(a);
    TagGetter<1>().get(b);
    TagGetter<2>().get(b);

    TagGetter<1>().get(c);
    TagGetter<2>().get(c);

    size_t x = sizeof(decltype(a.GetSymbol()));
    cout << x << endl;
    x = sizeof(decltype(b.GetSymbol()));
    cout << x << endl;

    return 0;
}
